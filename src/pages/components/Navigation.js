import React, { useState } from "react";
import { Menu, Button, Icon } from "antd";
import Link from "next/link";
import styled from "styled-components";
import PButton from "./PButton";
const { SubMenu } = Menu;
const StyledDiv = styled.div`
    justify-content: space-between;
    display: -webkit-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    padding-left: 10px;
    position: fixed;
    top: 4%;
    left: 0;
    right: 0;
    background: transparent;
    z-index: 999;
    margin-left: 20px;
    margin-right: 20px;
    align-items: center;
    display: flex;
    @media screen and (max-width: 700px) {
        font-size: 40px;
        left: 0%;
        .logo {
            display: none;
        }
    }
    .ant-menu-horizontal {
        background: transparent;
        color: white;
        border-bottom: none;
        width: 100%;
    }
    .logo {
        border-radius: 14px;
        vertical-align: middle;
        margin-left: auto;
        width: 120px;
        background: #999999;

        span {
            font-size: 16px;
        }
        .anticon-user {
            font-size: 16px;
            vertical-align: middle;
            float: left;
        }
    }
`;

export default function Navigation() {
    const [menu, setMenu] = useState("home");

    function handleOnclick(e) {
        console.log(e.key);
        setMenu(e.key);
    }
    return (
        <StyledDiv>
            <Menu
                mode="horizontal"
                defaultSelectedKeys={menu}
                onClick={handleOnclick}
            >
                <Menu.Item key="home">
                    <Link href="/">
                        <span>Home</span>
                    </Link>
                </Menu.Item>
                <SubMenu key="city" title="The City">
                    <Menu.Item key="mission">
                        <span>Mission and Vision</span>
                    </Menu.Item>
                    <Menu.Item key="best">
                        <span>Best Practices</span>
                    </Menu.Item>
                </SubMenu>
                <Menu.Item key="gov">
                    <Link href="/government">
                        <span>Goverment</span>
                    </Link>
                </Menu.Item>
                <Menu.Item key="contact">
                    <span>Contact Us</span>
                </Menu.Item>
            </Menu>
            <PButton name="Login" icon="user"></PButton>
            {/* <Button type="primary" className="logo"></Button> */}
        </StyledDiv>
    );
}
