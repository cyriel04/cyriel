import React from "react";
import { Button, Icon } from "antd";
import styled from "styled-components";

const StyledButton = styled(Button)`
    background: transparent;
    height: 40px;
    width: 200px;
    border-radius: 22px;
    border-color: black;
    color: black;
    margin: 20px;
    font-size: 18px;
`;

export default function PButton(props) {
    return (
        <div>
            <StyledButton type="primary">
                {props.name}
                {props.icon ? <Icon type={props.icon} /> : ""}
            </StyledButton>
        </div>
    );
}
