import { Carousel } from "antd";
import styled from "styled-components";

const StyledDiv = styled(Carousel)`
    height: 90vh;
    width: 100%;
    object-fit: cover;
    .ant-carousel {
        height: 90vh;
        width: 100%;
        object-fit: cover;
        .slick-list {
            height: 90vh;
            width: 100%;
            object-fit: cover;
            .slick-track {
                height: 90vh;
                width: 100%;
                object-fit: cover;
                .slick-slide {
                    border: none;
                    width: 100%;
                    object-fit: cover;
                    height: 90vh;
                    h1 {
                        display: flex;
                        position: fixed;
                        top: 5%;
                        left: 20%;
                        color: #d9d9d9;
                        font-size: 80px;
                        width: 640px;
                        font-weight: 900;
                        line-height: 100px;
                        @media screen and (max-width: 700px) {
                            font-size: 40px;
                            left: 10%;
                            width: 400px;
                        }
                    }
                    img {
                        width: 100%;
                        object-fit: cover;
                        height: 90vh;
                    }
                }
            }
        }
    }
`;

class PCarousel extends React.Component {
    render() {
        return (
            <StyledDiv>
                <Carousel autoplay dots={true} dotPosition="left" fade="fade">
                    <div>
                        <img src="static/code.jpg" />
                        <h1>This is a sample Text</h1>
                        {/* <h1>Book a Mobile Carwash</h1> */}
                    </div>
                    <div>
                        <img src="static/quick.jpg" />
                        <h1>We are the Champions</h1>
                        {/* <h1>Book a Mobile Carwash</h1> */}
                    </div>
                </Carousel>
            </StyledDiv>
        );
    }
}

export default PCarousel;
